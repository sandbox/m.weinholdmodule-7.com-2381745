<?php

/**
 * @file
 * Defines the core functions of the module
 */

/**
 * Creates a new Translation Aid Entity Revision.
 * If this is the first entity of a language for the entity it will create a new translation aid entity.
 * If a translation aid entity for this language and source entity already exists, it will create a new revision based on
 * the revision indicated by $base_vid or on the latest revision if this parameter is null.
 *
 * @param string $entity_type
 *   The type of the entity to translate
 * @param integer $entity
 *   The entity to translate
 * @param string $lang_code
 *   Destination language of this entity
 * @param translator
 *   The user in charge of the translation
 * @param integer $original_vid
 *   The vid of the revision of the node to translate
 * @param string $base_vid
 *   The vid of the translation aid entity on which we are basing this translation
 * @param integer $user
 *   The user creating this entity
 *
 * @todo Take into account the origin
 */
function translation_aid_create_entity($entity_type, $entity, $lang_code, $translator, $original_vid = 0, $base_vid = 0, $user = null) {
  // Load the user
  if (!$user) {
    global $user;
  }
  $user = user_load($user->uid);

  /*
   * General checks
   */
  // Check if the language is one of the configured in the site
  $languages = language_list();
  if (isset($languages[$lang_code])) {
    $language = $languages[$lang_code];
  }
  else {
    $options = array('@lang_code' => $lang_code);
    $message = t('@lang_code is not a configured language of the site.', $options);
    watchdog('translation_aid', $message, $options, WATCHDOG_INFO);
    drupal_set_message($message, 'error');
    return FALSE;
  }

  // Check if the user has permission to create a translation of this type
  if (!TranslationAid::isOperationAllowed(TranslationAid::OP_CREATE, $lang_code, $user)) {
    $options = array('@language' => $language->name);
    $message = t('Creation of translation to @language denied. Couldn\'t the Translation Object', $options);
    watchdog('translation_aid', $message, $options, WATCHDOG_INFO);
    drupal_set_message($message, 'error', FALSE);
    return FALSE;
  }

  // Check if the translator has permissions make translations of this language
  if (!TranslationAid::isUserTranslator($lang_code, $translator)) {
    // The assigned user is not a translator of this language
    $options = array(
          '@user' => $translator->name != '' ? $translator->name : t('The selected user '),
          '@language' => $language->name);
    $message = t('@user is not a translator of @language. Couldn\'t the Translation Object', $options);
    watchdog('translation_aid', $message, $options, WATCHDOG_INFO);
    drupal_set_message($message, 'error', FALSE);
    return FALSE;
  }

  // Get the entity data
  list($entity_id, $entity_vid, $entity_bundle) = entity_extract_ids($entity_type, $entity);
  $entity_title = entity_label($entity_type, $entity);

  // Check if the Translation Aid Kit is enabled for this entity
  if (!TranslationAid::isTranslationEnabled($entity_type, $entity)) {
    $message = t('Translation Aid Kit is not enabled for this entity.');
    watchdog('translation_aid', $message, array(), WATCHDOG_INFO);
    drupal_set_message($message, 'error');
    return FALSE;
  }
  /*
   * End of general checks
   */

  /*
   * Translation creation
   */
  // Get the entities to process
  // Check if we have to follow the entities tree
  if (TranslationAid::getFollowEntityTree()) {
    $entities = TranslationAid::getReferencedEntities($entity_type, $entity);
  }
  else {
    $entities = array(
      array(
        'entity_type' => $entity_type,
        'entity' => $entity,
        'entity_id' => $entity_id,
        'entity_vid' => $entity_vid,
      ),
    );
  }

  // Set the batch job configuration
  $batch = array(
    'operations' => array(
      array('translation_aid_treat_entities', array($entities, $language, $translator, $user)),
    ),
    'title' => t('Creating a new @lang_name Translation Object for @entity_title.',
        array('@lang_name' => $language->name, '@entity_title' => $entity_title)),
    'finished' => '_translation_aid_batch_finished',
  );

  // Send the job to the batch queue
  batch_set($batch);

  // Process the batch job
  if (!isset($destination)) {
    $destination = drupal_get_destination();
  }

  batch_process($destination);
}

/**
 * Treats the entities that were extracted from the source entity in a previous step of the batch
 * job launched in translation_aid_create_entity.
 *
 * @param array $entites
 *   An array with the entities to treat
 * @param mixed $language
 *   The language object of the language to which to translate the entities
 * @param mixed $translator
 *   The user object of the user to whom the translation is assigned
 * @param mixed $user
 *   The user creating the translation
 * @param array $context
 *   The batch context array
 *   @see translation_aid_get_entity_tree
 *   @see https://api.drupal.org/api/drupal/includes!form.inc/group/batch/7
 */
function translation_aid_treat_entities($entites, $language, $translator, $user, $context) {
  // Initialize the processed entities count
  if (!isset($context['sandbox']['processed_entities'])) {
    $context['sandbox']['processed_entities'] = 0;
    $context['sandbox']['entity_count']= count($entites);
    $context['sandbox']['entities'] = $entites;
  }
  $processed_entities = &$context['sandbox']['processed_entities'];
  $entity_count = $context['sandbox']['entity_count'];


  // Get the source entity to process and remove it from the context
  $source_entities = &$context['sandbox']['entities'];

  if (!empty($source_entities)) {
    $source_entity_data = array_shift($source_entities);
    $source_entity_id = $source_entity_data['entity_id'];
    $source_entity_vid = $source_entity_data['entity_vid'];
    $source_entity = $source_entity_data['entity'];
    $source_entity_type = $source_entity_data['entity_type'];
    $source_entity_bundle = $source_entity_data['entity_bundle'];
  }
  else {
    // We shouldn't come here, but just in case
    $context['finished'] = 1;
    return;
  }

  if (TranslationAid::isTranslationEnabled($source_entity_type, $source_entity)) {
    // Different initializations
    $ta_entity_type = 'translation_aid';
    $ta_bundle = 'translation_aid';
    $source_lang_code = entity_language($source_entity_type, $source_entity);
    $trans_lang_code = $language->language;

    // Get the translation_aid
    $translation_aid = TranslationAid::getTranslationAid($source_entity_type, $source_entity, $language, $source_entity_vid, $translator, $user, TRUE);
    $w_translation_aid = entity_metadata_wrapper($ta_entity_type, $translation_aid);
    // Set the language
    $w_translation_aid->language($trans_lang_code);

    // Get a wrapper for the source entity
    $w_source_entity = entity_metadata_wrapper($source_entity_type, $source_entity);


    // Get the fields info
    $properties_info = $w_source_entity->getPropertyInfo();

    // Search for translatable fields
    foreach ($properties_info as $field_name => $info) {
      if (isset($info['field']) && isset($info['translatable']) && $info['field'] && $info['translatable']) {
        $field_value = $w_source_entity->{$field_name}->value();
        /*
         * Add the original field to the translation aid entity
         */
        // Get the information of the correspondant field in our translation aid entity
        $tak_field_name = _translation_aid_get_field($field_name, $source_entity_type, $source_entity_bundle, TRUE, $w_translation_aid);

        // Set the value
        $w_translation_aid->{$tak_field_name}->set($field_value);
      }
    }

    // Save the translation aid object
    entity_get_controller('translation_aid')->save($translation_aid);
  }

  if (empty($source_entities)) {
    // Save the translation entity
    $context['finished'] = 1;
    $context['results']['message'] = t('@lang_name Translation Objects for @entity_title tree successfully created.',
        array('@lang_name' => $language->name, '@entity_title' => $context['results']['entity_title']));
    $context['results']['result'] = 'info';
    $finished = 1;
  }
  else {
    $finished = empty($source_entities) ? 1 : $processed_entities / $entity_count;
  }

  $processed_entities++;
  $context['message'] = t('Processed @processed_entities entities of @entity_count',
      array('@processed_entities' => $processed_entities, '@entity_count' => $entity_count));
  $context['finished'] = $finished;
}

/**
 * Checks if the entity was already in the array
 *
 * @param string $entity_type
 * @param mixed $entity
 * @param array $entities
 */
function _translation_aid_entity_in_tree($entity_type, $entity, $array) {
  list($entity_id,,) = entity_extract_ids($entity_type, $entity);

  foreach ($array as $value) {
    if ($entity_type == $value['entity_type'] && $entity_id == $value['entity_id']) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Returns a translation aid object for an entity and language
 * If a translation aid object already exists it loads it and the returns it, if not
 * returns new one
 *
 * @param string $entity_type
 *   The type of the entity
 * @param integer $entity_id
 *   The id of the entity
 * @param integer $entity_vid
 *   The vid of the entity
 * @param string/mixed $language
 *   The language code or the language object of the translation to return
 * @param mixed $translator
 *   The user object of the translator in charge of this translation
 * @param mixed $user
 *   The user object of the creator of the translation object
 * @param boolean $create
 *   If TRUE it will create a new translation object in case in does not exists yet one
 * @return mixed
 *   The translation_aid loaded or just created, or FALSE if there is already no translation and $create=FALSE
 */
function _translation_aid_get_translation_aid($entity_type, $entity, $language, $entity_vid = 0, $translator = NULL, $user = NULL, $create = FALSE) {

  list($entity_id, ,) = entity_extract_ids($entity_type, $entity);

  // Get the lang_code
  if ($language instanceof stdClass) {
    $lang_code = $language->language;
  }
  else {
    $lang_code = $language;
  }

  // First we have to check if the Translation Aid is enabled for this entity type
  if (TranslationAid::isTranslationEnabled($entity_type, $entity)) {
    // Check if a previous translation aid entity already exists
    $query = db_select('translation_aid', 'ta')
    ->fields('ta', array('taid', 'vid', 'entity_type', 'entity_id', 'original_entity_vid', 'status'))
    ->condition('entity_type', $entity_type, '=')
    ->condition('entity_id', $entity_id, '=')
    ->condition('language', $lang_code, '=');

    // Check if we look for a specific entity revision id
    if ($entity_vid) {
      // Get the specific one
      $query->condition('original_entity_vid', $entity_vid, '=');
    }

    $result = $query->execute()
    ->fetchAllAssoc('taid');

    if (empty($result)) {
      if ($create) {
        // We must create a new Translation Aid Entity
        $translation_aid = entity_get_controller('translation_aid')->create();
        // Add the basic properties
        $translation_aid->title = TranslationAid::getEntityTitle($entity_type, $entity_id, $lang_code);
        $translation_aid->type = 'translation_aid';
        $translation_aid->language = $lang_code;
        $translation_aid->entity_type = $entity_type;
        $translation_aid->entity_id = $entity_id;
        $translation_aid->original_entity_vid = $entity_vid;
        // This is the first revision so there is no base_vid
        $translation_aid->base_vid = 0;
        $translation_aid->uid = $user->uid;
        $translation_aid->translator = $translator->uid;
        $translation_aid->last_change_uid = $user->uid;
        $translation_aid->status = TranslationAid::ST_REQUESTED;
        $translation_aid->created = REQUEST_TIME;
        $translation_aid->changed =  REQUEST_TIME;
        $translation_aid->is_new = TRUE;

        return $translation_aid;
      }
    }
    else {
      // We already have one translation object so we load it and return it

      if (count($result) > 1) {
        // Search the most recent version of the original_entity_vid
        $max_vid = 0;
        $max_index = 0;
        foreach ($result as $delta => $record) {
          if ($max_vid < $record->original_entity_vid) {
            $max_vid = $record->original_entity_vid;
            $max_index = $delta;
          }
        }

        $record = $result[$max_index];
      }
      else {
        $record = reset($result);
      }
      $record = reset($result);
      $translation_aid = entity_revision_load(TranslationAid::ENTITY_TYPE, $record->vid);

      return $translation_aid;
    }
  }

  return FALSE;
}

/**
 * Returns the information of the field of our translation aid entity that corresponds to the given field
 * If the field instance (or the field itself) has not yet been created, we create it
 *
 * In some cases we just add a field instance of the original field to our entity, but sometimes this is not possible
 * (i.e. if there is a limited entity types allowed to add it) so we create an exact copy of the field to use it
 * with our translation aid entity
 *
 * @param string $field_name
 *   The name of the field in the source
 * @param string $source_entity_type
 *   The entity type we have to create the field/field_instance from
 * @param string $source_entity_bundle
 *   The bundle we have to create the field/field_instance from
 * @param boolean $create
 *   If we have to create the field or only return the its name
 * @param mixed $w_translation_aid
 *   An wrapper for the translation aid passed by reference. We have to update it if we add a new
 *   field to the translation_aid entity
 * @return string
 *   The name of the correspondant field in the translation aid entity
 */
function _translation_aid_get_field($field_name, $source_entity_type = '', $source_entity_bundle = '', $create = FALSE, &$w_translation_aid = null) {
  // Load the field mappings
  $result = db_select('translation_aid_mapping', 'm')
    ->fields('m')
    ->condition('field_name', $field_name, '=')
    ->execute()
    ->fetchAllAssoc('field_name');

  if (!$create) {
    // We are reading a value so we only need the name of the field
    return !empty($result) ? $result[$field_name]->tak_field_name : $field_name;
  }
  else {
    // We are creating a new translation aid object and we have to check if we have to create the field
    if (!empty($result) && $result[$field_name]->created) {
      // We have already the mapping so we simply return the our field_name
      return $result[$field_name]->tak_field_name;
    }
    else {
      $tak_field_name = $field_name;
      $ta_entity_type = TranslationAid::ENTITY_TYPE;
      $ta_bundle = TranslationAid::BUNDLE;
      // Load the field info
      $field_info = field_info_field($field_name);
      // Check if we can add this field
      if (!empty($field_info['entity_types']) && !in_array($ta_entity_type, $field_info['entity_types'])) {
        // Get our name for the field
        $tak_field_name = TranslationAid::getTakFieldName($field_name);
        // Check if have already created this field
        $tak_field = field_info_field($tak_field_name);
        if (empty($tak_field)) {
          // We must create our copy of the field
          $tak_field = field_info_field($field_name);
          $tak_field['field_name'] = $tak_field_name;
          $tak_field['entity_types'] = array($ta_entity_type);
          // Unset unnecessary settings
          unset($tak_field['storage']);
          unset($tak_field['foreign keys']);
          unset($tak_field['indexes']);
          unset($tak_field['id']);
          unset($tak_field['columns']);
          unset($tak_field['bundles']);

          field_create_field($tak_field);
        }
      }

      // Check if we already have the field instance
      $instance = field_info_instance($ta_entity_type, $tak_field_name, $ta_bundle);
      if (empty($instance)) {
        $instance = field_info_instance($source_entity_type, $field_name, $source_entity_bundle);
        $instance['field_name'] = $tak_field_name;
        $instance['entity_type'] = $ta_entity_type;
        $instance['bundle'] = $ta_bundle;
        $instance['required'] = FALSE;
        unset($instance['id']);
        unset($instance['field_id']);
        // Create the instance
        $instance = field_create_instance($instance);

        // Clear the field cache
        field_cache_clear();

        // Update the entity metadatawrapper of the translation aid
        $w_translation_aid = entity_metadata_wrapper($ta_entity_type, $w_translation_aid->value());
        $w_translation_aid->language($w_translation_aid->value()->language);
      }

      $record = array(
        'field_name' => $field_name,
        'tak_field_name' => $tak_field_name,
        'created' => TRUE,
      );

      drupal_write_record('translation_aid_mapping', $record);

      // We have to clear the config cache
      TranslationAid::clearCachedData();

      return $tak_field_name;
    }
  }
}

/**
 * Deletes a translation aid revision
 *
 * @param mixed $translation_aid_rev
 *   The translation aid revision to delete
 * @param mixed $user
 *   The user who deletes the translation aid
 * @param string $log_message
 *   A message to include in the log
 *
 * @todo pass this code to the entity controller class, this should be only a functional callback
 */
function translation_aid_delete_revision($translation_aid, $user = NULL) {
  if (!$user) {
    global $user;
  }
  $user = user_load($user->uid);

  $taid = $translation_aid->taid;
  $del_vid = $translation_aid->vid;

  // Check if the user has permission to delete a translation_aid object
  if (TranslationAid::isOperationAllowed(TranslationAid::OP_DELETE, $translation_aid->language, $translation_aid, $user)) {
    // Check if this is the default revision
    $translation_aid_current = translation_aid_load($taid);
    $current_vid = $translation_aid_current->vid;

    if ($current_vid == $del_vid) {
      // Check for earlier revisions
      $old_vid = db_select('translation_aid_revision', 'tar')
      ->fields('tar', array('vid'))
      ->condition('taid', $taid, '=')
      ->condition('vid', $del_vid, '!=')
      ->orderBy('created', 'DESC')
      ->execute()
      ->fetchField();

      if ($old_vid) {
        // Set the old revision as the default for the entity
        $ta_old_revision = translation_aid_load($taid, $old_vid);
        entity_revision_set_default('translation_aid', $ta_old_revision);
        entity_get_controller('translation_aid')->save($ta_old_revision);

        // Delete the revision
        entity_get_controller('translation_aid')->deleteRevision($del_vid);
      }
      else {
        // This is the only revision of the entity so we delete it completely
        entity_get_controller('translation_aid')->delete(array($taid));
      }
    }
    else {
      // This is not the default revision of the entity
      // Delete the revision
      entity_get_controller('translation_aid')->deleteRevision($del_vid);
    }

    module_invoke_all('translation_aid_delete', $translation_aid, $user);
  }
  else {
    watchdog(
    'translation_aid',
    '@user_name does not have permission to delete translation aid entities.',
    array('@user_name' => $user->name),
    WATCHDOG_INFO
    );
    return FALSE;
  }
}

/**
 * Writes a new entry in the translation aid record
 *
 * @param mixed $translation_aid
 * @param mixed $user
 * @param string $type
 * @param string $message
 */
function translation_aid_add_log_entry($translation_aid, $user, $type = 'message', $message) {
  $created = REQUEST_TIME;
  $record = array(
    'taid' => $translation_aid->taid,
    'vid' => $translation_aid->vid,
    'language' => $translation_aid->language,
    'entity_type' => $translation_aid->entity_type,
    'entity_id' => $translation_aid->entity_id,
    'uid' => $user->uid,
    'type' => $type,
    'message' => $message,
    'created' => $created,
  );

  drupal_write_record('translation_aid_log', $record);
}


/**
 * Deletes all the temporary object for all the translation aids
 */
function translation_aid_clear_all_temp() {
  // Load all the translation aid objects
  $translation_aids = translation_aid_load_multiple();

  if (!empty($translation_aids)) {
    foreach ($translation_aids as $translation_aid) {
      translation_aid_clear_temp($translation_aid);
    }
  }
}

/**
 * Deletes all the temporary objects of a translation aid
 *
 * @param mixed $translation_aid
 * @return boolean
 */
function translation_aid_clear_temp($translation_aid) {
  // Check if the user has permission to edit the data
  global $user;

  if (!translation_aid_access('update', $translation_aid->taid, $user)) {
    return FALSE;
  }

  // Delete the records
  $result = db_delete('translation_aid_temp')
  ->condition('taid', $translation_aid->taid, '=')
  ->condition('vid', $translation_aid->vid, '=')
  ->execute();

  return $result > 0 ? TRUE : FALSE;
}

/**
 * Stores the value of a variable in a temporal storage of the module
 * TODO introduce some checks
 *
 * @param mixed $translation_aid
 * @param string $key_id
 * @param mixed $value
 */
function translation_aid_set_temp($translation_aid, $key_id, $value) {
  $taid = $translation_aid->taid;
  $vid = $translation_aid->vid;

  // Check if a record already exists
  $num_rows = db_select('translation_aid_temp', 'temp')
  ->fields('temp', array($taid))
  ->condition('taid', $taid, '=')
  ->condition('vid', $vid, '=')
  ->condition('key_id', $key_id, '=')
  ->countQuery()
  ->execute()
  ->fetchField();

  if ($num_rows > 0) {
    // The key already exists, we must update it
    $num_updated = db_update('translation_aid_temp')
    ->condition('taid', $taid, '=')
    ->condition('vid', $vid, '=')
    ->condition('key_id', $key_id, '=')
    ->fields(array(
      'value' => serialize($value),
      'changed' => REQUEST_TIME,
    ))
    ->execute();

  }
  else {
    // The key does not exists, we must insert it
    $query = db_insert('translation_aid_temp')
    ->fields(array(
      'taid' => $taid,
      'vid' => $vid,
      'key_id' => $key_id,
      'value' => serialize($value),
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    ))
    ->execute();
  }
}

/**
 * Removes an entry from the temporary form storage
 *
 * @param mixed $translation_aid
 * @param string $key_id
 */
function translation_aid_remove_temp($translation_aid, $key_id) {
  $taid = $translation_aid->taid;
  $vid = $translation_aid->vid;

  $num_deleted = db_delete('translation_aid_temp')
  ->condition('taid', $taid, '=')
  ->condition('vid', $taid, '=')
  ->condition('key_id', $key_id, '=')
  ->execute();
}

/**
 * Returns the log entries based on certain conditions
 *
 * @param array $conditions
 */
function translation_aid_get_log_entries($conditions = array(), $pager = FALSE, $header = null) {
  // Build the query and get the result
  $query = db_select('translation_aid_log', 'log')
  ->fields('log', array('lid', 'taid', 'vid', 'entity_type', 'entity_id', 'language', 'uid', 'type', 'message', 'created'));

  // Set the conditions
  foreach ($conditions as $condition) {
    $query->condition($condition['field'], $condition['value'], $condition['operation']);
  }

  // Set the header of the order if is set

  // Check if we have to use a pager
  if ($pager) {
    if (isset($header)) {
      $limit = TranslationAidConfig::getLogPagerLimit();
      $query
      ->extend('TableSort')
      ->orderByHeader($header)
      ->extend('PagerDefault')
      ->limit($limit);
    }
    else {
      $query->orderBy('created', 'DESC');
      $query->extend('PagerDefault')
      ->limit(TranslationAidConfig::getLogPagerLimit());
    }

  }

  $result = $query
  ->execute();

  return $result;
}

/**
 * Assigns the translation to a translator
 *
 * @param mixed $taid
 *   Id of the translation aid to assign
 * @param mixed $translator
 */
function translation_aid_assign_translation($taid, $translator) {
  global $user;

  // Load the translation
  $translation_aid = reset(entity_load(TranslationAid::ENTITY_TYPE, array($taid)));

  // Check if the translation exists
  if ($translation_aid) {
    // Language of the translation
    $language = $translation_aid->language;

    // Check if the translator user can translate this language and the user is allow to assing it
    if (in_array($translator->uid, array_keys(TranslationAid::getTranslators($language))) &&
      TranslationAid::isOperationAllowed(TranslationAid::OP_ASSIGN, $language, $translation_aid, $user)) {
      // Update the translation and save it
      $translation_aid->translator = $translator->uid;
      entity_get_controller('translation_aid')->save($translation_aid);

      return TRUE;
    }
  }

  return TRUE;
}

/**
 * Sets the translation as approved
 *
 * @param mixed $translation_aid
 * @param mixed $user
 * @param string $message
 * @return boolean
 */
function translation_aid_approve_translation($translation_aid, $user) {
  // Load the entity
  list($entity_type, $entity,,) = TranslationAid::getEntitydata($translation_aid);
  $title = entity_label($entity_type, $entity);

  $language = translation_aid_get_language_name($translation_aid);

  // Save the translation aid
  entity_get_controller('translation_aid')->approve($translation_aid);

  // Feedback message
  $message = t('@lang_name Translation for @title successfully updated.',
      array('@lang_name' => $language, '@title' => $title));

  return TRUE;
}

/**
 * Rejects a translation
 *
 * @param mixed $translation_aid
 * @param string $log_message
 * @return boolean
 */
function translation_aid_reject_translation($translation_aid, $user) {
  // Load the entity
  list($entity_type, $entity,,) = TranslationAid::getEntitydata($translation_aid);
  $title = entity_label($entity_type, $entity);

  $language = translation_aid_get_language_name($translation_aid);

  // Save the translation aid
  $translation_aid->log_message = $log_message;
  entity_get_controller('translation_aid')->reject($translation_aid);

  // Feedback message
  $message = t('@lang_name Translation for @title successfully updated.',
      array('@lang_name' => $language, '@title' => $title));

  return TRUE;
}

/**
 * Publishes a translation
 *
 * @param mixed $translation_aid
 * @param mixed $user
 * @param string $log_message
 */
function translation_aid_publish_translation($translation_aid, $user, $message = '') {
  $trans_lang = $translation_aid->language;

  // Check if the language is one of the configured in the site
  if (in_array($trans_lang, array_keys(language_list()))) {
    // Check if the operation is allowed
    if (TranslationAid::isOperationAllowed(TranslationAid::OP_PUBLISH, $trans_lang, $translation_aid, $user)) {
      // Get a wrapper for the translation_aid
      $w_translation_aid = entity_metadata_wrapper(TranslationAid::ENTITY_TYPE, $translation_aid);
      $w_translation_aid->language($trans_lang);
      $tak_field_instances = field_info_instances(TranslationAid::ENTITY_TYPE, TranslationAid::BUNDLE);

      // Get the original entity data
      list($entity_type, $entity, $entity_id, $entity_vid) = TranslationAid::getEntityData($translation_aid);
      list(, , $entity_bundle) = entity_extract_ids($entity_type, $entity);

      // Get an entity translation handler
      $et_handler = entity_translation_get_handler($entity_type, $entity, TRUE);

      // Check that the translation language is not the same as the source language of the entity
      $translations = $et_handler->getTranslations();
      $source_language = $translations->original;
      if ($trans_lang != $source_language) {
        $w_entity = entity_metadata_wrapper($entity_type, $entity);
        $w_entity->language($trans_lang);

        // Get the fields of the entity
        $field_instances = field_info_instances($entity_type, $entity_bundle);

        // Check if this entity has already a translation to this language

        if (!in_array($trans_lang, array_keys($translations->data))) {
          // We have to create the translation
          $translation = array(
            'translate' => 0,
            'status' => 1,
            'language' => $trans_lang,
            'source' => $source_language,
            'uid' => $user->uid,
          );

          $et_handler->setTranslation($translation, array());
          $et_handler->saveTranslations();
        }

        foreach ($field_instances as $field_name => $field_instance) {
          $field_info = field_info_field($field_name);

          // Check if the field is translatable
          if ($field_info['translatable']) {
            // Get the tak field
            $tak_field_name = _translation_aid_get_field($field_name);
            if (isset($w_translation_aid->{$tak_field_name})) {
              $value = $w_translation_aid->{$tak_field_name}->value();
              if ($value) {
                $w_entity->{$field_name}->set($value);
              }
            }
          }
        }
        //Save the entity
        $w_entity->save();

        // Set the translation as published
        entity_get_controller(TranslationAid::ENTITY_TYPE)->updateStatus($translation_aid, TranslationAid::ST_PUBLISHED);
      }
      else {
        drupal_set_message(t('There was an error when trying to create the translation.'), 'error');
      }

    }
    else {
      drupal_set_message(t('Translation publication not allowed.'), 'error');
    }
  }
  else {
    drupal_set_message(t('Language not configured in the site.', 'error'));
  }
}

/**
 * Helper function to prepare and launch a process in a batch job
 *
 * @param array $values
 *   Array with the values to process
 * @param unknown $options
 *   Array with different options for the set up of the batch job
 *     'callback' - Callback function to call
 *     'destination' - Destination to redirect after the batch is processed
 *     'process_title' - The title of the process to show in the batch title page
 *     'progress_message' - Progress message
 *     'finish_message' - Finish message to show if success
 *     'error_message' - Message to show if there is an error while processing the job
 *     'replacements' - Array with replacements for the messages
 * @param array $args
 *   The additonal arguments to pass to the callback
 */
function _translation_aid_launch_batch($values, $options, $args = array()) {
  // Check for the required parameters
  // General checks for aborting requirents
  if (isset($options['callback'])) {
    $callback = $options['callback'];
    $process_title = isset($options['process_title']) ? $options['process_title'] : 'Processing batch job...';
    $replacements = isset($options['replacements']) ? $options['replacements'] : array();
    $params = array($values, $options, $args);

    // Set the batch job configuration
    $batch = array(
      'operations' => array(
        array('_translation_aid_batch', $params),
      ),
      'title' => t($process_title, $replacements),
      'finished' => '_translation_aid_batch_finished',
    );

    // Send the job to the batch queue
    batch_set($batch);

    // Process the batch job
    $destination = isset($options['destination']) ? $options['destination'] : drupal_get_destination();

    batch_process($destination);
  }
  else {
    $error_message = isset($options['error_message']) ? $options['error_message'] : 'Process could not be initialized. Process aborted.';
    $replacements = isset($options['replacements']) ? $options['replacements'] : array();
    drupal_set_message(t($error_message, $replacements), 'error');
  }
}

/**
 * Helper callback to run a process in a batch job
 *
 * @param array $values
 * @param array $options
 *   @see _translation_aid_launch_batch
 * @param mixed $args
 *   The additonal arguments to pass to the callback
 * @param array $context
 *   The batch context array
 *   @see translation_aid_get_entity_tree
 *   @see https://api.drupal.org/api/drupal/includes!form.inc/group/batch/7
 */
function _translation_aid_batch($values, $options, $args, &$context) {
  $abort = FALSE;

  // General checks for aborting requirents
  if (isset($options['callback'])) {
    $callback = $options['callback'];
  }
  else {
    $abort = TRUE;
  }

  // Check if there is a reason for aborting the batch job
  if ($abort) {
    // Set an error message and return
    $error_message = isset($options['error_message']) ? $options['error_message'] : 'Process could not be initialized. Batch job aborted.';
    $context['finished'] = 1;
    $context['results']['message'] = t($error_message);
    $context['results']['result'] = 'error';
    $finished = 1;

    return;
  }

  // Initialize the sandbox
  if (!isset($context['sandbox']['processed_values'])) {
    $context['sandbox']['processed_values'] = 0;
    $context['sandbox']['total_values']= count($values);
    $context['sandbox']['values'] = $values;
    $context['sandbox']['progress_message'] = isset($options['progress_message']) ? $options['progress_message'] : 'Processed @processed_values of @total_count.';
    $context['sandbox']['replacements'] = isset($options['replacements']) ? $options['replacements'] : array();
    // Add the total and processed values to the replacements
    $context['sandbox']['replacements']['@processed_values'] = $context['sandbox']['processed_values'];
    $context['sandbox']['replacements']['@total_values'] = $context['sandbox']['total_values'];
  }

  $processed_values = &$context['sandbox']['processed_values'];
  $total_values = $context['sandbox']['total_values'];

  $values = &$context['sandbox']['values'];
  if (!empty($values)) {
    // Get the next value to process and remove it from the context
    $value = array_shift($values);
    // Call the processing function
    $args = !empty($args) ? array_merge(array($value), $args) : array($value);
    call_user_func_array($callback, $args);
  }

  // Check if we have finished the batch job
  if (!empty($values)) {
    // Update the messages and go on
    $processed_values++;
    $context['sandbox']['replacements']['@processed_values'] = $processed_values;
    $context['message'] = t($context['sandbox']['progress_message'], $context['sandbox']['replacements']);
    $finished = empty($values) ? 1 : $processed_entities / $total_values;
    $context['finished'] = $finished;
  }
  else {
    $context['finished'] = 1;
    $finish_message = isset($options['finish_message']) ? $options['finish_message'] : 'Process finished successfully. Processed @total_count elements.';
    $context['results']['message'] = t($finish_message, $context['sandbox']['replacements']);
    $context['results']['result'] = 'info';
    $context['finished'] = 1;
  }
}

/**
 * Callback function for the batch finish
 *
 *   @see callback_batch_finished
 */
function _translation_aid_batch_finished($success, $results, $operations) {
  // We return a message to the user
  drupal_set_message($results['message'], $results['result']);
}