<?php

/**
 * @file
 * Adds a diff element comparing the versions of the
 */

/**
 *  *******************
 *   Implemented hooks
 *  *******************
 */

/**
 * Implements hook_menu().
 */
function translation_aid_diff_menu() {
  $items = array();

  $items['admin/config/regional/translation-aid/diff'] = array(
    'title' => t('Diff integration'),
    'description' => t('Diff integration settings.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('translation_aid_diff_admin_form'),
    'access arguments' => array('administer translation aid'),
    'file' => 'translation_aid_diff.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_libraries_info
 */
function translation_aid_diff_libraries_info() {
  return array(
    'finediff' => array(
      'name' => 'Finediff',
      'vendor url' => 'http://www.raymondhill.net/finediff/viewdiff-ex.php',
      'download url' => 'http://www.raymondhill.net/finediff/finediff-code.php',
      'version arguments' => array(
        'file' => 'finediff-code.php',
        'pattern' => "/@version\s+([0-9\.]+)/",
        'lines' => 40,
      ),
      'files' => array(
        'php' => array(
          'finediff-code.php',
        ),
      ),
    ),
  );
}

/**
 * Implements hook_field_info_alter
 *
 *  We include the TranslationAidDiffController information for the basic field types
 */
function translation_aid_diff_field_info_alter(&$info) {
  foreach ($info as $field_type => &$field_type_info) {
    switch ($field_type) {
      case 'text':
      case 'text_long':
      case 'text_with_summary':
        $field_type_info['settings'] += array(
          'translation_aid_diff_controller' => 'TranslationAidDiffDefaultTextFieldController',
        );
        break;
    }
  }
}

/**
 * Implements hook_translation_aid_edit_form_element_alter
 */
function translation_aid_diff_translation_aid_edit_form_element_alter(&$form_element, &$changed, $context) {
  // Get the field_name and delta
  $field_name = $context['field_name'];

  // Get the diff controller and see if the field type supports translation_aid_diff
  $diff_controller = translation_aid_diff_get_controller($field_name);
  if ($diff_controller) {
    // Get the translation aids and source entities
    $translation_aid = $context['translation_aid'];
    $tak_field_name = _translation_aid_get_field($field_name);
    list($entity_type, $entity, $entity_vid, $entity_bundle) = TranslationAid::getEntityData($translation_aid);
    $et_handler = entity_translation_get_handler($entity_type, $entity);
    $source_language = $et_handler->getTranslations()->original;
    if ($translation_aid->base_vid != 0) {
      $translation_aid_base = entity_revision_load(TranslationAid::ENTITY_TYPE, $translation_aid->base_vid);
      list(, $base_entity, $base_entity_vid, ) = TranslationAid::getEntityData($translation_aid_base);
    }

    // Get the field value
    $values = isset($entity->{$field_name}[$source_language]) ? $entity->{$field_name}[$source_language] : array();
    foreach ($form_element[$tak_field_name] as $delta => &$field_element) {
      if (!preg_match('/^#/', $delta)) {
        if (isset($values[$delta])) {
          $value = $values[$delta];
          $value_base = isset($base_entity) ? $base_entity->{$field_name}[$source_language][$delta] : array();
          $field_element['original']['content'] = $diff_controller->getDiffElement($entity_type, $entity, $field_name, $value_base, $value, $source_language);
          $changed = $changed || !$diff_controller->equals($value_base, $value);

          // Add a class to style the diff field
          if (!isset($field_element['original']['#attributes']['class'])) {
            $field_element['original']['#attributes']['class'] = array();
          }

          $field_element['original']['#attributes']['class'][] = 'ta-diff';
        }
      }
    }

    drupal_add_css(drupal_get_path('module', 'translation_aid_diff') . '/css/translation_aid_diff.css');
  }
}

/**
 * Returns a TranslationAidDiffController for a field type to generate the diff values and check if two values are equal
 *
 * @param string $field_type
 * @throws Exception
 * @return TranslationAidControllerInterface
 *   A class implementing TranslationAidControllerInterface.
 */
function translation_aid_diff_get_controller($field_name) {
  $field_info = field_info_field($field_name);

  if (isset($field_info['settings']['translation_aid_diff_controller'])) {
    try {
      return new $field_info['settings']['translation_aid_diff_controller']();
    }
    catch (Exception $ex) {
      $message = 'Couldn\'t get a controller for the field @field_name';
      $options = array('@field_type' => $field_name);
      watchdog('translation_aid_diff', $message, $options, WATCHDOG_ERROR);
      throw $ex;
    }
  }
  else {
    $message = 'Couldn\'t get a controller for the field @field_name. Field type not supported';
    $options = array('@field_type' => $field_name);
    watchdog('translation_aid_diff', $message, $options, WATCHDOG_DEBUG);
  }
}

/**
 * Settings class definition
 */
class TranslationAidDiff {
  /*
   * Constant definitions
   */
  // Diff granularities
  const GR_CHARACHTER = 'character';
  const GR_WORD = 'word';
  const GR_SENTENCE = 'sentence';
  const GR_PARAGRAPH = 'paragraph';

  private static $default_diff_granularity = 'character';

  private static $available_granularities = array(
    self::GR_CHARACHTER => array('finediff granularity' => 'characterGranularity'),
    self::GR_WORD => array('finediff granularity' => 'wordGranularity'),
    self::GR_SENTENCE => array('finediff granularity' => 'sentenceGranularity'),
    self::GR_PARAGRAPH => array('finediff granularity' => 'paragraphGranularity'),
  );

  public static function getDiffGranularity() {
    return variable_get('tak_diff_granularity', self::$default_diff_granularity);
  }

  public static function getAvailableGranularities() {
    $return = array();
    foreach (array_keys(self::$available_granularities) as $granularity) {
      $return[$granularity] = ucfirst($granularity);
    }

    return $return;
  }

  public static function getFineDiffGranularity() {
    return self::$available_granularities[self::getDiffGranularity()]['finediff granularity'];
  }
}