<?php

/**
 * @file
 * Defines the admin forms and suppoting functions used in the Translation Aid Module configuration
 */

/**
 * Callback function for the settings form
 *
 * @param $form
 * @param $form_state
 * @return
 *   The system settings form structure
 *
 * @todo
 *   Add a clear temp data button or remove it when this option is disabled
 */
function translation_aid_settings_form($form, &$form_state) {

  /*
   * For which entity types will be the Translation Aid Kit enabled
   */
  // Get the possible entities (those with entity translation enabled)
  $options = array();
  foreach (entity_get_info() as $entity_type => $info) {
    if (entity_translation_enabled($entity_type)) {
      $options[$entity_type] = !empty($info['label']) ? t($info['label']) : $entity_type;
    }
  }

  /*
   * General options
   */
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General options'),
    '#description' => t('Configure the general options of the Translation Aid Kit module.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Enabled types
  $enabled_types = TranslationAid::getEnabledEntityTypes();
  $form['general']['tak_enabled_entity_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Translation Aid Enabled Entity Types'),
    '#description' => t('Select the entities that will have the Translation Aid Kit enabled'),
    '#options' => $options,
    '#default_value' => $enabled_types,
  );

  // Follow the entity tree
  $form['general']['tak_follow_entity_tree'] = array(
    '#type' => 'checkbox',
    '#title' => t('Follow the entities tree'),
    '#description' => t('If enabled will search recursively for entities referenced not translatable fields.'),
    '#default_value' => TranslationAid::getFollowEntityTree(),
  );

  // Hide Entity Translation edit forms
  $form['general']['tak_hide_entity_translation_forms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Entity Translation Forms'),
    '#description' => t('If enabled will hide the Entity Translation forms and Translation Aid Kit will be the only entry point to translation.'),
    '#default_value' => TranslationAid::getHideEntityTranslationForms(),
  );

  /*
   * Logging options
   */
  $form['logging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logging'),
    '#description' => t('Configure the logging options for the Translation Aid Kit module.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['logging']['tak_log_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Logging enabled'),
    '#default_value' => TranslationAid::getLogActive(),
  );

  if (TranslationAid::getLogActive()) {
    $form['logging']['tak_required_log_messages'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Required log messages'),
      '#description' => t('Select the operation that will require a mandatory log message.'),
      '#options' => TranslationAid::getLogMessages(),
      '#default_value' => TranslationAid::getRequiredLogMessages(),
    );
  }

  $form = system_settings_form($form);

  // Add a custom submit function
  $form['#submit'] = array_merge(array('translation_aid_settings_form_submit'), $form['#submit']);

  return $form;
}

/**
 * Callback function for the settings form of the edition form
 *
 * @param $form
 * @param $form_state
 * @return
 *   The system settings form structure
 *
 */
function translation_aid_edit_form_settings_form($form, &$form_state) {
  /*
   * Edit form Options
   */
  $form['edit_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Edit form options'),
    '#description' => t('Configure the options for the edit form of the Translation Aid Kit module.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['edit_form']['tak_show_original_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show original content'),
    '#description' => t('If enabled, show the content in the original language in above the edit field.'),
    '#default_value' => TranslationAid::getSaveTemporaryFormEntries(),
  );

  $form['edit_form']['tak_original_content_collapsed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapse original content fieldset'),
    '#description' => t('If enabled, shows the original content fieldset collapsed as default when the edition form opens.'),
    '#default_value' => TranslationAid::getOriginalContentCollapsed(),
  );

  $form['edit_form']['tak_links_to_referenced_entities'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show links to referenced entities'),
    '#description' => t('If enabled, shows links to view the entity in the original language, and edit the current language translation of the referenced entities.'),
    '#default_value' => TranslationAid::getShowLinksToReferencedEntities(),
  );

  $form['edit_form']['tak_save_temporary_form_entries'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable save temporary data from form fields'),
    '#description' => t('If enabled, uses javascript to save the values of modified form fields when leaving the field focus.'),
    '#default_value' => TranslationAid::getShowOriginalContent(),
  );

  $form['edit_form']['tak_fieldset_collapse_state'] = array(
    '#type' => 'select',
    '#title' => t('Default collapse state of the fieldsets'),
    '#description' => t('Select which from the collapsible fieldsets of the edit form will be open by default.'),
    '#options' => TranslationAid::getFieldsetCollapseStates(),
    '#default_value' => TranslationAid::getFieldsetCollapseConfiguredState(),
  );

  return system_settings_form($form);
}

/**
 * Additional submit callback for the admin form
 *
 * @param mixed $form
 * @param mixed $form_state
 *
 * @todo
 * Clear the cache only when it's really needed
 */
function translation_aid_settings_form_submit($form, $form_state) {
  // We have to clear the cache to regenerate the menu entries
  drupal_flush_all_caches();

}