<?php

/**
 * @file
 * Defines an interface an a default implementation for the getting diff elements for fields
 */

interface TranslationAidDiffControllerInterface {

  /**
   * Checks if the two values are equal
   *
   * @param array $value_old
   *   Array structure representing old value of the field.
   * @param array $value_new
   *   Array structure representing new value of the field.
   * @return boolean
   *   TRUE if the two values are equal, FALSE if not
   */
  public function equals($value_old, $value_new);

  /**
   * Generates and returns a renderable diff element based in the two input parameters
   *
   * @param string $entity_type
   *   The entity type of the field for which we have to generate the diff element.
   * @param mixed $entity
   *   The entity of the field for which we have to generate the diff element.
   * @param string $field_name
   *   The name of the field
   * @param array $value_old
   *   Array structure representing old value of the field.
   * @param array $value_new
   *   Array structure representing new value of the field.
   * @param string $langcode
   *   The language of the value
   * @return array
   *   A renderable array representing the diff between the two input parameters
   */
  public function getDiffElement($entity_type, $entity, $field_name, $value_old, $value_new, $langcode);

}

class TranslationAidDiffDefaultTextFieldController implements TranslationAidDiffControllerInterface {

  /**
   * {@inheritdoc}
   */
  public function equals($value_old, $value_new) {
    $equal = TRUE;

    if (isset($value_new['value']) || isset($value_old['value'])) {
      // Compare the value
      $new_value = isset($value_new['value']) ? $value_new['value'] : '';
      $old_value = isset($value_old['value']) ? $value_old['value'] : '';

      $equal = ($new_value === $old_value);

      // Compare the summary if we have it and the normal value has not changed
      if ($equal && (isset($value_new['summary']) || isset($value_old['summary']))) {
        $summary_new = isset($value_new['summary']) ? $value_new['summary'] : '';
        $summary_old = isset($value_old['summary']) ? $value_old['summary'] : '';

        $equal = ($summary_new === $summary_old);
      }
    }
    else {
      $equal = ($value_new === $value_old);
    }

    return $equal;
  }

  /**
   * {@inheritdoc}
   */
  public function getDiffElement($entity_type, $entity, $field_name, $value_old, $value_new, $langcode) {
    $diff_value = $value_new;
    if (isset($diff_value['value'])) {
      // Unset the safe values if they are set
      foreach (array_keys($diff_value) as $key) {
        if (preg_match('/^safe_/', $key)) {
          unset($diff_value[$key]);
        }
      }

      // Get the diff value
      $value_to = $value_new['value'];
      $value_from = !empty($value_old) ? $value_old['value'] : '';
      $diff_value['safe_value'] = nl2br($this->getDiff($value_from, $value_to));

      // Get the summary diff if it's set
      if (isset($diff_value['summary'])) {
        $summary_to = $value_new['summary'];
        $summary_from = !empty($value_old) ? $value_old['summary'] : '';
        $diff_value['safe_summary'] = nl2br($this->getDiff($value_from, $value_to));
      }
    }
    else {
      $diff_value = $this->getDiff($value_old, $value_new);
    }

    return field_view_value($entity_type, $entity, $field_name, $diff_value, array(), $langcode);
  }

  /**
   * Custom renderer function for the FineDiff library
   *
   * @param string $opcode
   * @param string $from
   * @param integer $from_offset
   * @param integer $from_len
   */
  public static function renderDiff($opcode, $from, $from_offset, $from_len) {
    if ( $opcode === 'c' ) {
      echo htmlentities(substr($from, $from_offset, $from_len));
    }
    else if ( $opcode === 'd' ) {
      $deletion = substr($from, $from_offset, $from_len);
      if ( strcspn($deletion, " \n\r") === 0 ) {
        $deletion = str_replace(array("\n","\r"), array('\n','\r'), $deletion);
      }
      echo '<del>', htmlentities($deletion), '</del>';
    }
    else /* if ( $opcode === 'i' ) */ {
   			echo '<ins>', htmlentities(substr($from, $from_offset, $from_len)), '</ins>';
    }
  }

  private function getDiff($from_text, $to_text) {
    // Load the diff engine library
    libraries_load('finediff');

    $granularity = TranslationAidDiff::getFineDiffGranularity();

    $opcodes = FineDiff::getDiffOpcodes($from_text, $to_text, FineDiff::$$granularity);

    ob_start();
    FineDiff::renderFromOpcodes($from_text, $opcodes, array('TranslationAidDiffDefaultTextFieldController', 'renderDiff'));

    return ob_get_clean();
  }
}