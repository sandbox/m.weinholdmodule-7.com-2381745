(function($){
  Drupal.behaviors.translationAid = {
    attach: function(context, settings) {
      // Aux function to show a message after an Ajax request
      var showMessage = function (element, message) {
        messageDiv = '<div class="ajax-feedback">' + message + '</div>';
        element.after(messageDiv);
      };
      
      // Process each row of the languages table
      $("form#translation-aid-overview-form table.translation-overview tbody tr").each(function() {
        // Get the language
        var row = $(this);
        var language = row.attr('language');
        var taid = row.attr('taid');
        var select = row.find("select.assigned-translator");
        var button = row.find("input.assign-translator:button");
        var createLink = row.find("a.tak-create-link");
        
        // Attach the click function to the assign button
        button.click(function() {
          // Hide the button
          $(this).hide();
          
          // Show the Ajax progress indicator
          
          // Get the translator value
          var translator = select.val();
          
          // Make the Ajax request to assign the translator
          $.ajax({
            type: "GET",
            url: settings.basePath + "translation_aid/" + taid + "/assign/" + translator,
            contentType: "application/json; charset=utf-8",
            success: function( data, textStatus, jqXHR ) {
              console.log('assigned ' + language + ' translation to user ' + translator);
              showMessage(select, 'Assignment updated');
            },
            error: function( jqXHR, textStatus, errorThrown ) {
              console.log(textStatus);
            },
          });
        });
        
        // Attach the change handler function to the translator select
        select.change(function() {
          // Show the assign button if there is already a translation
          if ( !row.hasClass("no-translation") ) {
            button.show();
          }
          
          // Remove the empty option if exists
          $(this).find("option.empty").remove();
          
          // Update the destination of the creation link
          var translator = select.val();
          var overviewUrl = settings.translationAid.ta_base_path;
          var query = 'translator=' + translator + '&destination=' + overviewUrl;
          var url = settings.basePath + overviewUrl + "/create/" + language + "?" + query;
          createLink.attr("href", url);
          // Show the creation link if hidden
          createLink.show();
        });        
      });
    }
  }
  
})(jQuery);