<?php 

/**
 * @file
 * Default theme implementation for the editable assigned translator field 
 * 
 */

$id = $element['#id'];
$options = $element['#options'];
$default_value = $element['#default_value'] ? $element['#default_value'] : 0;

// Add javascript and css files
if (isset($element['#attached']['css'])) {
  $css_files = $element['#attached']['css'];
  foreach ($css_files as $css_file) {
    drupal_add_css($css_file);
  }
}

if (isset($element['#attached']['js'])) {
  $js_files = $element['#attached']['js'];
  foreach ($js_files as $js_file) {
    drupal_add_js($js_file);
  }
}

?>

<div>
  <select id="<?php print $id; ?>" class="form-select assigned-translator">
    <?php // If we have no default value, include an empty one 
      if ($default_value == 0):
    ?>
      <option value selected="selected" class="empty">--</option>
    <?php endif; ?>
    <?php foreach ($options as $value => $option): ?>
      <option value="<?php print $value; ?>"<?php print $default_value == $value ? 'selected' : ''; ?>><?php print $option; ?></option>
    <?php endforeach; ?>
  </select>
  <input type="button" id="btn-<?php print $id; ?>" value="Assign" class="form-submit assign-translator" style="display: none">
</div>