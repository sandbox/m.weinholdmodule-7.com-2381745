<?php
/**
 * @file
 * 
 * Definition of the view to show the translations assigned to a translator
 */

$view = new view();
$view->name = 'my_translations';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'translation_aid';
$view->human_name = 'My translations';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'My translations';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
/* No results behavior: Global: Unfiltered text */
$handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
$handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
$handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
$handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
$handler->display->display_options['empty']['area_text_custom']['content'] = 'You currently have no translations assigned to you';
/* Field: Translation Aid: Translation Aid ID */
$handler->display->display_options['fields']['taid']['id'] = 'taid';
$handler->display->display_options['fields']['taid']['table'] = 'translation_aid';
$handler->display->display_options['fields']['taid']['field'] = 'taid';
/* Field: Translation Aid: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'translation_aid';
$handler->display->display_options['fields']['title']['field'] = 'title';
/* Field: Translation Aid: State */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'translation_aid';
$handler->display->display_options['fields']['status']['field'] = 'status';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['alter']['text'] = 'edit';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['nothing']['alter']['path'] = 'translation_aid/[taid]/edit?destination=my-translations';
/* Contextual filter: Translation Aid: Translator uid */
$handler->display->display_options['arguments']['translator']['id'] = 'translator';
$handler->display->display_options['arguments']['translator']['table'] = 'translation_aid';
$handler->display->display_options['arguments']['translator']['field'] = 'translator';
$handler->display->display_options['arguments']['translator']['default_action'] = 'default';
$handler->display->display_options['arguments']['translator']['default_argument_type'] = 'current_user';
$handler->display->display_options['arguments']['translator']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['translator']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['translator']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Translation Aid: State */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'translation_aid';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = array(
  0 => '0',
  1 => '1',
  3 => '3',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'my-translations';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$translatables['my_translations'] = array(
  t('Master'),
  t('My translations'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('You currently have no translations assigned to you'),
  t('Translation Aid ID'),
  t('.'),
  t(','),
  t('Title'),
  t('State'),
  t('Custom text'),
  t('edit'),
  t('All'),
  t('Page'),
);


$views[$view->name] = $view;