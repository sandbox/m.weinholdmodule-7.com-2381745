<?php

/**
 * Defines a Class that holds the configuration information and related functions for the Translation Aid Kit module
 */

/**
 * Configuration class
 */
class TranslationAid {
  /*
   * Constants definition
   */
  // Translation state
  const ST_REQUESTED = 'requested';
  const ST_IN_PROGRESS = 'in_progress';
  const ST_COMPLETED = 'completed';
  const ST_REJECTED = 'rejected';
  const ST_APPROVED = 'approved';
  const ST_PUBLISHED = 'published';

  // Translation Aid Base Path
  const TA_BASE_PATH = 'translation_aid';

  // Default operations
  const OP_ADMINISTER = 'admin';
  const OP_CREATE = 'create';
  const OP_EDIT = 'edit';
  const OP_DELETE = 'delete';
  const OP_ACCEPT = 'accept';
  const OP_ASSIGN = 'assign';
  const OP_PUBLISH = 'publish';
  const OP_EDIT_ALL = 'edit_all';

  // Default log events
  const LOG_CREATE = 'create';
  const LOG_SAVE = 'save';
  const LOG_COMPLETE = 'complete';
  const LOG_APPROVE = 'approve';
  const LOG_REJECT = 'reject';
  const LOG_PUBLISH = 'publish';
  const LOG_DELETE = 'delete';
  const LOG_DELETE_REVISION = 'delete_revision';
  const LOG_INSERT = 'insert';
  const LOG_UPDATE = 'update';

  // Default Fieldset collapse states
  const FS_NONE_OPEN = 0;
  const FS_FIRST_CHANGED_OPEN = 1;
  const FS_ALL_CHANGED_OPEN = 2;
  const FS_ALL_OPEN = 3;

  // Entity Type and Bundle
  const ENTITY_TYPE = 'translation_aid';
  const BUNDLE = 'translation_aid';

  /*
   * Private properties
   */
  private static $default_operations = array(
    self::OP_ADMINISTER => array(
      'permission' => 'administer translation aid',
      'title' => 'Administer Translation Aid',
      'description' => 'Administer the Translation Aid Toolkit entities and fields.',
    ),
    self::OP_CREATE => array(
      'permission' => 'create translation aid',
      'title' => 'Create Translation Aid Entities',
      'description' => 'Create new Translation Aid entities.',
    ),
    self::OP_EDIT => array(
      'permission' => 'edit translation aid',
      'title' => 'Edit Translation Aid Entities.',
      'description' => 'Edit assigned Translation Aid entities',
    ),
    self::OP_DELETE => array(
      'permission' => 'delete translation aid',
      'title' => 'Delete Translation Aid Entities.',
      'description' => 'Delete the Translation Aid entities',
    ),
    self::OP_ACCEPT => array(
      'permission' => 'accept translation aid',
      'title' => 'Accept Translation Aid Entities',
      'description' => 'Approve or reject a translation.',
    ),
    self::OP_ASSIGN => array(
      'permission' => 'assign translation aid',
      'title' => 'Assign Translation Aid Entities',
      'description' => 'Assign a translation to a translator.',
    ),
    self::OP_PUBLISH => array(
      'permission' => 'publish translation aid',
      'title' => 'Publish Translation Aid Entities',
      'description' => 'Publish a translation.',
    ),
    self::OP_EDIT_ALL => array(
      'permission' => 'edit all translation aid',
      'title' => 'Edit all Translation Aid Entities',
      'description' => 'Edit all the Translation Aid Entities of the assigned languages.',
    ),
  );

  private static $default_entity_title = '!entity_title - !language';

  private static $default_translation_states = array(
    self::ST_REQUESTED => 'Translation requested',
    self::ST_IN_PROGRESS => 'Translation in progress',
    self::ST_COMPLETED => 'Translation completed',
    self::ST_REJECTED => 'Translation rejected',
    self::ST_APPROVED => 'Translation approved',
    self::ST_PUBLISHED => 'Translation published',
  );

  private static $default_editable_translation_states = array(
    self::ST_REQUESTED,
    self::ST_IN_PROGRESS,
    self::ST_REJECTED,
  );

  private static $default_notify_only_source_types_changes = TRUE;

  private static $default_log_pager_limit = 20;

  private static $default_save_temporary_form_entries = TRUE;

  private static $default_log_active = FALSE;

  private static $default_log_events = array(
    self::LOG_CREATE => FALSE,
    self::LOG_SAVE => FALSE,
    self::LOG_APPROVE => FALSE,
    self::LOG_REJECT => TRUE,
    self::LOG_PUBLISH => FALSE,
    self::LOG_DELETE => TRUE,
  );

  private static $default_log_messages = array(
    self::LOG_CREATE => '!language translation object of !title created.',
    self::LOG_COMPLETE => '!language translation object of !title completed.',
    self::LOG_APPROVE => '!language translation object of !title approved.',
    self::LOG_REJECT => '!language translation object of !title rejected.',
    self::LOG_PUBLISH => '!language translation object of !title published.',
    self::LOG_DELETE => '!language translation object of !title deleted.',
    self::LOG_DELETE_REVISION => 'Revision of !language translation object of !title deleted.',
    self::LOG_INSERT => '!language translation object of !title created.',
    self::LOG_UPDATE => '!language translation object of !title updated.',
  );

  private static $default_required_log_messages = array(

  );

  private static $default_enabled_entity_types = array(
    'node',
  );

  private static $default_follow_entity_tree = TRUE;

  // If we should show the original content in the edition form
  private static $default_show_original_content = TRUE;

  // If the original content fieldset should be collapsed when opening the edition form
  private static $default_original_content_collapsed = FALSE;

  // If we should show link to the referenced entities in the edition form
  private static $default_links_to_referenced_entities = TRUE;

  // If we have to hide the entity translation forms
  private static $default_hide_entity_translation_forms = FALSE;

  // The options for the collapsible field sets
  private static $fieldset_collapse_states = array(
    self::FS_NONE_OPEN => 'All fieldset collapsed',
    self::FS_FIRST_CHANGED_OPEN => 'Open the fieldset of the first changed value',
    self::FS_ALL_CHANGED_OPEN => 'Open the fieldset of all the changed values',
    self::FS_ALL_OPEN => 'All fieldsets opened',
  );

  private static $default_fieldset_collapse_state = self::FS_FIRST_CHANGED_OPEN;

  /*
   * Getters
   */
  public static function getOperations() {
    return self::$default_operations;
  }

  public static function getDefaultEntityTitle() {
    return variable_get('tak_entity_title', self::$default_entity_title);
  }

  public static function getEntityTitle($entity_type, $entity, $language) {

    return t('testing title');

    // TODO: refine the title generation
    $w_node = entity_metadata_wrapper('node', $node);
    switch ($node->type) {
      case 'hands_on_guide':
        $entity_title = drupal_html_to_text($w_node->field_hog_translatable_title->value());
        break;
      case 'how_to_booklet_chapter':
        $entity_title = drupal_html_to_text($w_node->field_htb_translatable_title->value());
        break;
      default:
        $entity_title = drupal_html_to_text($w_node->title->value());
        break;
    }
    return t(
        variable_get('tak_entity_title', self::$default_entity_title),
        array(
          '!entity_title' => $entity_title,
          '!entity_nid' => $node->nid,
          '!language' => strtoupper($language),
        )
    );
  }

  public static function getTranslationStates() {
    return variable_get('tak_translation_states', self::$default_translation_states);
  }

  public static function getEditableTranslationStates() {
    return variable_get('tak_editable_translation_states', self::$default_editable_translation_states);
  }

  public static function getExcludedFields () {
    return variable_get('tak_excluded_fields', self::$default_excluded_fields);
  }

  public static function notifyOnlySourceTypesChanges() {
    return variable_get('tak_notify_only_source_types_changes', self::$default_notify_only_source_types_changes);
  }

  public static function getLogPagerLimit() {
    return variable_get('tak_log_pager_limit', self::$default_log_pager_limit);
  }

  public static function getSaveTemporaryFormEntries() {
    return variable_get('tak_save_temporary_form_entries', self::$default_save_temporary_form_entries);
  }

  public static function getLogActive() {
    return variable_get('tak_log_active', self::$default_log_active);
  }

  public static function getLogMessages() {
    $messages = array();
    foreach (array_keys(self::$default_log_events) as $message) {
      $messages[$message] = ucfirst($message);
    }

    return $messages;
  }

  public static function getRequiredLogMessages() {
    $required_messages = variable_get('tak_required_log_messages', self::$default_log_events);

    return array_keys(array_filter($required_messages));
  }

  public static function getEnabledEntityTypes() {
    return variable_get('tak_enabled_entity_types', self::$default_enabled_entity_types);
  }

  public static function getLogMessage($event) {
    $log_messages = variable_get('tak_log_messages', self::$default_log_messages);

    return isset($log_messages[$event]) ?  $log_messages[$event] : '';
  }

  public static function getFollowEntityTree() {
    return variable_get('tak_follow_entity_tree', self::$default_follow_entity_tree);
  }

  public static function getShowOriginalContent() {
    return variable_get('tak_show_original_content', self::$default_show_original_content);
  }

  public static function getOriginalContentCollapsed() {
    return variable_get('tak_original_content_collapsed', self::$default_original_content_collapsed);
  }

  public static function getShowLinksToReferencedEntities() {
    return variable_get('tak_links_to_referenced_entities', self::$default_links_to_referenced_entities);
  }

  public static function getHideEntityTranslationForms() {
    return variable_get('tak_hide_entity_translation_forms', self::$default_hide_entity_translation_forms);
  }

  public static function getFieldsetCollapseStates() {
    return self::$fieldset_collapse_states;
  }

  public static function getFieldsetCollapseConfiguredState() {
    return variable_get('tak_fieldset_collapse_state', self::$default_fieldset_collapse_state);
  }

  /**
   * Returns the base path for the translation aid for an entity type
   *
   * @param string $entity_type
   * @return array
   */
  public static function getTypeBasePaths($entity_type) {
    $entity_info = entity_get_info($entity_type);

    $ta_base_path = '';
    $entity_base_path = '';
    if (isset($entity_info['translation']['entity_translation']['path schemes'])) {
      $entity_base_path = $entity_info['translation']['entity_translation']['path schemes']['default']['base path'];
      $base_path = $entity_base_path . '/' . self::TA_BASE_PATH;
    }

    return array($entity_base_path, $base_path);
  }

  /**
   * Returns the base path for the translation aid for an entity
   *
   * @param string $entity_type
   * @param mixed $entity
   * @return array
   *   The paths.
   */
  public static function getEntityBasePaths($entity_type, $entity) {
    $base_path = entity_uri($entity_type, $entity);
    $ta_base_path = $base_path['path'] . '/' . self::TA_BASE_PATH;

    return array($base_path, $ta_base_path);
  }

  public static function isLogMessageRequired($operation) {
    $required_messages = self::getRequiredLogMessages();

    return in_array($operation, $required_messages);
  }

  /**
   * Returns an array with the uid of the users with permission to translate a language
   *
   * @param string $language
   * @return mixed
   */
  public static function getTranslators($language) {
    // Edit translations permission
    $edit_perm = self::$default_operations[self::OP_EDIT]['permission'];

    // Build the query
    $query = db_select('users_roles', 'ur');
    $query->join('users', 'u', 'ur.uid=u.uid');
    $query->join('role', 'r', 'ur.rid=r.rid');
    $query->join('role_permission', 'rp', 'r.rid=rp.rid');
    $query->join('field_data_field_tak_translator_languages', 'tl', 'ur.uid=tl.entity_id and tl.entity_type=\'user\'');
    $result = $query->fields('ur', array('uid'))
      ->fields('u', array('name'))
      ->condition('rp.permission', $edit_perm, '=')
      ->condition('tl.field_tak_translator_languages_value', $language, '=')
      ->orderBy('u.name', 'ASC')
      ->execute()
      ->fetchAllKeyed();

    return $result;
  }

  /**
   * Tells if the translation is editable based on its state and the user
   *
   * @param boolean $state
   */
  public static function isTranslationEditable($translation_aid, $user = NULL) {
    // Get the defined states
    $editable_states = self::getEditableTranslationStates();

    if (in_array($translation_aid->status, array_keys($editable_states))) {
      return TranslationAid::isOperationAllowed(TranslationAid::OP_EDIT, $translation_aid->language, $translation_aid, $user);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns the name of the cloned fields
   *
   * @param string $field_name
   */
  public static function getTakFieldName($field_name) {
    $pattern = "/^field_(.*)/";

    return preg_match_all($pattern,$field_name,$matches) ? 'tak_' . $matches[1][0] : 'tak_' . $field_name;
  }

  /**
   * Returns the node types that some translation aid may refer to
   *
   * @return mixed
   */
  public static function getReferencedNodeTypes() {
    $referenced_node_types = &drupal_static(__FUNCTION__);

    if (!isset($referenced_node_types)) {
      if ($cache = cache_get('referenced_node_types')) {
        $referenced_node_types = $cache->data;
      }
      else {
        // Source types
        $referenced_node_types = self::getSourceTypes();

        // Get the referenced source types
        $field_instances = field_info_instances('translation_aid', 'translation_aid');
        foreach ($field_instances as $field_name => $field_instance) {
          $field_info = field_info_field($field_name);
          if ($field_info['type'] == 'entityreference') {
            $referenced_node_types = array_merge($referenced_node_types, array_values($field_info['settings']['handler_settings']['target_bundles']));
          }
        }
        // Set the cache
        cache_set('referenced_node_types', $referenced_node_types, 'cache');
      }
    }

    return $referenced_node_types;
  }

  /**
   * Returns the entity reference fields in use by the translation aid entity
   */
  public static function getEntityReferenceFields() {
    $used_entityreference_fields = &drupal_static(__FUNCTION__);

    if (empty($used_entityreference_fields)) {
      if ($cache = cache_get('used_entityreference_fields')) {
        $used_entityreference_fields = $cache->data;
      }
      else {
        $used_entityreference_fields = array();

        // Field used by the translation aid entity
        $field_instances = field_info_instances('translation_aid', 'translation_aid');
        foreach ($field_instances as $field_name => $field_instance) {
          $field_info = field_info_field($field_name);
          if ($field_info['type'] == 'entityreference') {
            $used_entityreference_fields[$field_name] = array(
              'field_info' => $field_info,
              'field_instance' => $field_instance,
            );
          }
        }
      }
    }

    return $used_entityreference_fields;
  }

  /**
   * Returns the list of languages the user has assigned
   *
   * @param object $user
   *   The user object
   * @return array
   *   An array with the languages
   */
  public static function getUserLanguages($user) {
    // Get a wrapper
    $w_user = entity_metadata_wrapper('user', $user);

    $values = $w_user->field_tak_translator_languages->value();

    $languages = array();
    if (!empty($values)) {
      foreach ($values as $value) {
        $languages[] = $value['value'];
      }
    }

    return $languages;
  }

  public static function clearCachedData() {
    // Clear the cached values
    if (cache_get('referenced_node_types')) {
      cache_clear_all('referenced_node_types');
    }
    if (cache_get('used_entityreference_fields')) {
      cache_clear_all('used_entityreference_fields');
    }
  }

  public static function getPermissions() {
    $operations = self::$default_operations;

    $permissions = array();
    foreach ($operations as $operation) {
      $permissions[$operation['permission']] = $operation;
      unset($permissions[$operation['permission']]['permission']);
    }

    return $permissions;
  }
  /**
   * Checks if the user is allowed to execute an operation based on his permissions and his assigned languages
   *
   * @param string $operation
   *   The operation to execute
   * @param string $language
   *   The language to check the permission against
   * @param mixed $translation_aid
   *   The translation aid to check the permission against
   * @return boolean
   *   If the user is allowed or not
   */
  public static function isOperationAllowed($operation, $language, $translation_aid = NULL, $user = NULL) {
    // Load the user if not set
    if (!$user) {
      global $user;
    }

    $operations = self::$default_operations;

    if (in_array($operation, array_keys($operations))) {
      // Check if the operation is one of the allowed for its state
      if ($translation_aid && $operation != TranslationAid::OP_CREATE) {
        if (!in_array($operation, self::getAllowedOperations($translation_aid->status))) {
          // Don't allow the operation if it's not in the right state
          return FALSE;
        }
      }

      // Check for administrative permission
      if (user_access($operations[self::OP_ADMINISTER]['permission'], $user)) {
        return TRUE;
      }

      // Check the edit permission for translators
      if ($operation == self::OP_EDIT && !user_access($operations[self::OP_EDIT_ALL]['permission'], $user)) {
        // The user has permission to edit only his translations
        if (!empty($translation_aid) && $translation_aid->uid == $user->uid) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }

      // Check if the user has the language assigned
      if (in_array($language, self::getUserLanguages($user))) {
        // Check if the user has the permission
        return user_access($operations[$operation]['permission'], $user);
      }
    }

    // In other cases don't grant access
    return FALSE;
  }

  /**
   * Checks if the user has permissions to edit translation of a language (those assigned to him or all)
   *
   * @param string $language
   *   The language to check
   * @param mixed $user
   *   The user object to check
   * @return boolean
   *   If the user is allowed or not
   */
  public static function isUserTranslator($language, $user = NULL) {
    // Load the user if not set
    if (!$user) {
      global $user;
    }

    $operations = self::$default_operations;

    return (user_access($operations[self::OP_ADMINISTER]['permission'], $user) ||
             (in_array($language, self::getUserLanguages($user)) &&
               (user_access($operations[self::OP_EDIT_ALL]['permission'], $user) ||
                user_access($operations[self::OP_EDIT]['permission'], $user))
             )
           );
  }

  /**
   * Checks if the Translation Aid Kit is active for this entity
   *   The type has to be checked in the settings page
   *   Entity Translation must be active for the bundle
   *
   * @param string $entity_type
   *   The entity type to check
   * @param mixed $entity
   *   The entity to check
   * @return boolean
   *   If it's or not activated the TAK
   */
  public static function isTranslationEnabled($entity_type, $entity) {
    // We must include the tab only on the enabled entity types
    $enabled_types = TranslationAid::getEnabledEntityTypes();
    if (in_array($entity_type, $enabled_types)) {
      // Check if this entity has any translatable field
      $translatable = FALSE;

      list(,, $bundle) = entity_extract_ids($entity_type, $entity);

      foreach (field_info_instances($entity_type, $bundle) as $instance) {
        $field_name = $instance['field_name'];
        $field = field_info_field($field_name);
        if ($field['translatable']) {
          $translatable = TRUE;
          break;
        }
      }
      return $translatable;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns the translation aids objects referenced through the original entities fields
   *
   * @param mixed $translation_aid
   * @param array $states
   *   An array with which to filter the returned translation aids
   * @return array
   *   An array of translation aid
   */
  public static function getReferencedTranslations($translation_aid, $states = array()) {
    // Get the original entity
    list($entity_type, $entity, $entity_id, $entity_vid) = self::getEntityData($translation_aid);

    // Get the referenced entities
    $entities = self::getReferencedEntities($entity_type, $entity);

    $translation_aids = array();
    $language = $translation_aid->language;

    // Clean the states array
    $filter = !empty($states);
    $states = array_intersect($states, array_keys(self::getTranslationStates()));

    foreach ($entities as $entity_data) {
      $temp_translation_aid = self::getTranslationAid($entity_data['entity_type'], $entity_data['entity'], $language);
      if ($temp_translation_aid) {
        if ($filter) {
          if (in_array($temp_translation_aid->status, $states)) {
            $translation_aids[$temp_translation_aid->taid] = $temp_translation_aid;
          }
        }
        else {
          // We don't have to filter by states
          $translation_aids[$temp_translation_aid->taid] = $temp_translation_aid;
        }
      }
    }

    return $translation_aids;
  }

  /**
   * Returns the entites referenced through the original entity fields in a recursive way
   *
   * @param mixed $entity_type
   * @param array $entity
   * @return array
   *   An array of entities
   */
  public static function getReferencedEntities($entity_type, $entity) {
    // Get the already processed entities
    $processed_entities = &drupal_static(__FUNCTION__);

    list($entity_id, $entity_vid, $entity_bundle) = entity_extract_ids($entity_type, $entity);

    // Add me to the entities list and to the processed entities list
    $entity_data = array(
      'entity_type' => $entity_type,
      'entity' => $entity,
      'entity_id' => $entity_id,
      'entity_vid' => $entity_vid,
    );

    $entities = array($entity_data);
    $processed_entities[] = $entity_data;

    // Get a wrapper for the entity
    $w_entity = entity_metadata_wrapper($entity_type, $entity);

    // Get the properties of the entity
    $properties = $w_entity->getPropertyInfo();

    foreach ($properties as $name => $info) {
      if (isset($info['field']) && $info['field']) {
        // Check if the field is translatable
        // If it's translatable, it's the same if it makes a reference to another entity because
        // the translated value will make a reference to another instance of the entity
        // The fields that reference entities that are translatable with entity translation should
        // be configured as not translatable, and make the translation in the referenced entity
        if (!isset($info['translatable']) || !$info['translatable']) {
          $w_field = $w_entity->{$name};
          switch(get_class($w_field)) {
            case 'EntityDrupalWrapper':
              // This is a referenced entity so we have to keep going
              $field_value = $w_field->value();
              $field_type = $w_field->type();

              // Check if this was already processed to prevent loops
              if (!self::isEntityInTree($field_type, $field_value, $processed_entities)) {
                // Recursive call to add the possible referenced entities
                $entities = array_merge($entities, self::getReferencedEntities($field_type, $field_value));
              }
              break;
            case 'EntityListWrapper':
              // We have a multivalued field, so we should check if it has entity references
              // Get a iterator and search for other entities
              $iterator = $w_field->getIterator();
              while ($iterator->valid()) {
                $current = $iterator->current();
                if (get_class($current) == 'EntityDrupalWrapper') {
                  // This is a referenced entity so we have to keep going
                  $field_value = $current->value();
                  $field_type = $current->type();

                  // Check if this was already processed to prevent loops
                  if (!self::isEntityInTree($field_type, $field_value, $processed_entities)) {
                    // Recursive call to add the possible referenced entities
                    $entities = array_merge($entities, self::getReferencedEntities($field_type, $field_value));
                  }
                }
                $iterator->next();
              }
              break;
          }
        }
      }
    }

    return $entities;
  }

  /**
   * Returns the original entity correspondant to a translation aid object
   * @param unknown $translation_aid
   *
   * @return array
   *  entity_type
   *  entity
   *  entity_id
   *  entity_vid
   *
   * @todo should we check if the entity is revisionable through entity_get_info??
   */
  public static function getEntityData($translation_aid) {
    // Get the basic data
    $entity_type = $translation_aid->entity_type;
    $entity_id = $translation_aid->entity_id;
    $entity_vid = $translation_aid->original_entity_vid;

    $entity = $entity_vid != 0 ? entity_revision_load($entity_type, $entity_vid) : reset(entity_load($entity_type, array($entity_id)));

    return array($entity_type, $entity, $entity_id, $entity_vid);
  }

  /**
   * Checks if the entity was already in the array
   *
   * @param string $entity_type
   * @param mixed $entity
   * @param array $entities
   */
  private static function isEntityInTree($entity_type, $entity, $array) {
    list($entity_id,,) = entity_extract_ids($entity_type, $entity);

    foreach ($array as $value) {
      if ($entity_type == $value['entity_type'] && $entity_id == $value['entity_id']) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns a translation aid object for an entity and language
   * If a translation aid object already exists it loads it and the returns it, if not
   * returns new one
   *
   * @param string $entity_type
   *   The type of the entity
   * @param integer $entity_id
   *   The id of the entity
   * @param integer $entity_vid
   *   The vid of the entity
   * @param string/mixed $language
   *   The language code or the language object of the translation to return
   * @param mixed $translator
   *   The user object of the translator in charge of this translation
   * @param mixed $user
   *   The user object of the creator of the translation object
   * @param boolean $create
   *   If TRUE it will create a new translation object in case in does not exists yet one
   * @return mixed
   *   The translation_aid loaded or just created, or FALSE if there is already no translation and $create=FALSE
   */
  public static function getTranslationAid($entity_type, $entity, $language, $entity_vid = 0, $translator = NULL, $user = NULL, $create = FALSE) {

    list($entity_id, ,) = entity_extract_ids($entity_type, $entity);

    // Get the lang_code
    if ($language instanceof stdClass) {
      $lang_code = $language->language;
    }
    else {
      $lang_code = $language;
    }

    // First we have to check if the Translation Aid is enabled for this entity type
    if (TranslationAid::isTranslationEnabled($entity_type, $entity)) {
      // Check if a previous translation aid entity already exists
      $query = db_select('translation_aid', 'ta')
      ->fields('ta', array('taid', 'vid', 'entity_type', 'entity_id', 'original_entity_vid', 'status'))
      ->condition('entity_type', $entity_type, '=')
      ->condition('entity_id', $entity_id, '=')
      ->condition('language', $lang_code, '=');

      // Check if we look for a specific entity revision id
      if ($entity_vid) {
        // Get the specific one
        $query->condition('original_entity_vid', $entity_vid, '=');
      }

      $result = $query->execute()
      ->fetchAllAssoc('taid');

      if (empty($result)) {
        if ($create) {
          // We must create a new Translation Aid Entity
          $translation_aid = entity_get_controller('translation_aid')->create();
          // Add the basic properties
          $translation_aid->title = TranslationAid::getEntityTitle($entity_type, $entity_id, $lang_code);
          $translation_aid->type = 'translation_aid';
          $translation_aid->language = $lang_code;
          $translation_aid->entity_type = $entity_type;
          $translation_aid->entity_id = $entity_id;
          $translation_aid->original_entity_vid = $entity_vid;
          // This is the first revision so there is no base_vid
          $translation_aid->base_vid = 0;
          $translation_aid->uid = $user->uid;
          $translation_aid->translator = $translator->uid;
          $translation_aid->last_change_uid = $user->uid;
          $translation_aid->status = TranslationAid::ST_REQUESTED;
          $translation_aid->created = REQUEST_TIME;
          $translation_aid->changed =  REQUEST_TIME;
          $translation_aid->is_new = TRUE;

          return $translation_aid;
        }
      }
      else {
        // We already have one translation object so we load it and return it

        if (count($result) > 1) {
          // Search the most recent version of the original_entity_vid
          $max_vid = 0;
          $max_index = 0;
          foreach ($result as $delta => $record) {
            if ($max_vid < $record->original_entity_vid) {
              $max_vid = $record->original_entity_vid;
              $max_index = $delta;
            }
          }

          $record = $result[$max_index];
        }
        else {
          $record = reset($result);
        }
        $record = reset($result);
        $translation_aid = entity_revision_load(TranslationAid::ENTITY_TYPE, $record->vid);

        return $translation_aid;
      }
    }

    return FALSE;
  }

  public function getDefaultFSCollapseState() {
    switch(self::getFieldsetCollapseConfiguredState()) {
      case self::FS_NONE_OPEN:
      case self::FS_FIRST_CHANGED_OPEN:
      case self::FS_ALL_CHANGED_OPEN:
        return TRUE;
        break;
      case self::FS_ALL_OPEN:
        return FALSE;
        break;
    }
  }

  /**
   * Returns the allowed operations for this translation state
   * @param $status
   *   The state to check
   * @return array
   *   array with the allowed operations for the input state
   */
  private static function getAllowedOperations($status) {
    switch ($status) {
      case TranslationAid::ST_REQUESTED:
      case TranslationAid::ST_IN_PROGRESS:
      case TranslationAid::ST_REJECTED:
        return array(
          TranslationAid::OP_ADMINISTER,
          TranslationAid::OP_ASSIGN,
          TranslationAid::OP_DELETE,
          TranslationAid::OP_EDIT,
          TranslationAid::OP_EDIT_ALL,
        );
        break;
      case TranslationAid::ST_COMPLETED:
        return array(
          TranslationAid::OP_ADMINISTER,
          TranslationAid::OP_DELETE,
          TranslationAid::OP_EDIT,
          TranslationAid::OP_EDIT_ALL,
          TranslationAid::OP_ACCEPT,
        );
        break;
      case TranslationAid::ST_APPROVED:
        return array(
          TranslationAid::OP_ADMINISTER,
          TranslationAid::OP_DELETE,
          TranslationAid::OP_PUBLISH,
        );
        break;
      case TranslationAid::ST_PUBLISHED:
        return array(
          TranslationAid::OP_ADMINISTER,
        );
        break;
      default:
        return array();
    }
  }
}