<?php

/**
 * @file
 * API documentation for the Translation Aid Kit module.
 */

function hook_translation_aid_delete($translation_aid) {

}

function hook_translation_aid_revision_delete($translation_aid) {

}

function hook_translation_aid_presave(&$translation_aid) {

}

function hook_translation_aid_insert($translation_aid) {

}

function hook_translation_aid_update($translation_aid) {

}

function hook_translation_aid_edit_form_element_alter(&$value, &$changed, $context) {

}