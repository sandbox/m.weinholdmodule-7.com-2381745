<?php

/**
 * @file
 * Defines the Entity Controller for the Translation Aid entity
 * Relies on the functionality implemented in EntityAPIController
 */

class TranslationAidController extends EntityAPIController {
  public function approve($translation_aid) {
    $translation_aid->status = TranslationAid::ST_APPROVED;
    $this->save($translation_aid);
  }

  public function reject($translation_aid) {
    $translation_aid->status = TranslationAid::ST_REJECTED;
    $this->save($translation_aid);
  }

  public function updateStatus($translation_aid, $status) {
    $status_list = TranslationAid::getTranslationStates();

    // Check if the status is one of the configured states
    if (in_array($status, array_keys($status_list))) {
      $translation_aid->status = $status;
      $this->save($translation_aid);
    }
  }

  /**
   * Override EntityAPIController::save
   */
  public function save($translation_aid, DatabaseTransaction $transaction = NULL) {
    global $user;

    // Set the last user that changed the entity
    $translation_aid->last_change_uid = $user->uid;

    module_invoke_all('translation_aid_presave', $translation_aid);

    // Call parent
    parent::save($translation_aid, $transaction);

    if (isset($translation_aid->is_new) && $translation_aid->is_new) {
      module_invoke_all('translation_aid_insert', $translation_aid);
    }
    else {
      module_invoke_all('translation_aid_update', $translation_aid);
    }
  }

  /**
   * Override EntityAPIController::delete
   */
  public function delete($taids, DatabaseTransaction $transaction = NULL) {
    global $user;

    $translation_aids = $taids ? $this->load($taids) : FALSE;

    if (!$translation_aids) {
      return;
    }

    // Call the delete function of the EntityAPIController
    parent::delete($taids, $transaction);

    // Invoke the hooks
    foreach ($translation_aids as $translation_aid) {
      // Set the user that deleted the translation aid
      $translation_aid->last_change_uid = $user->uid;
      module_invoke_all('translation_aid_delete', $translation_aid);
    }
  }

  /**
   * Override EntityAPIController::deleteRevision
   */
  public function deleteRevision($revision_id) {
    global $user;

    if ($translation_aid_revision = entity_revision_load($this->entityType, $revision_id)) {
      // Set the user that deleted the translation aid revision
      $translation_aid_revision->last_change_uid = $user->uid;
      if (parent::deleteRevision($revision_id)) {
        // Invoke the revision delete hook
        module_invoke_all('translation_aid_revision_delete', $translation_aid_revision);

        return TRUE;
      }
    }
    return FALSE;
  }
}