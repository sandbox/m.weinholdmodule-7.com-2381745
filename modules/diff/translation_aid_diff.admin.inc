<?php

/**
 * @file
 * Defines the admin forms and suppoting functions used in the Translation Aid Module configuration
 */

/**
 * Callback function for the settings form
 *
 * @param $form
 * @param $form_state
 * @return
 *   The system settings form structure
 */
function translation_aid_diff_admin_form($form, &$form_state) {
  $form = array();

  // Detect the finediff library
  $library = libraries_detect('finediff');

  if ($library['installed']) {
    /*
     * General options
     */
    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('General options'),
      '#description' => t('Configure the general options of the Translation Aid Kit Diff integration module.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    // Diff granularity
    $diff_granularities = TranslationAidDiff::getAvailableGranularities();
    $form['general']['tak_diff_granularity'] = array(
      '#type' => 'select',
      '#title' => t('Diff granularity'),
      '#description' => t('Select the granularity that will be used to calculate the diff between the texts.'),
      '#options' => $diff_granularities,
      '#default_value' => TranslationAidDiff::getDiffGranularity(),
    );
  }
  else {
    $message = '<b>@library library not detected.</b><br>Translation Aid Kit requires !link library to work correctly. Please place its <b>@file</b> file inside the <b>@library_path</b> directory';
    $replacements = array(
      '@library' => $library['name'],
      '!link' => filter_xss(l(t('FineDiff'), 'http://www.raymondhill.net/finediff/finediff-code.php')),
      '@file' => key($library['files']['php']),
      '@library_path' => $library['library path'],
    );
    drupal_set_message(t($message, $replacements), 'error');
  }

  return system_settings_form($form);
}