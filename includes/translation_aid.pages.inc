<?php

/**
 * @file
 * Defines different page callback function used in the module
 */

/**
 * Callback function for the creation menu entry
 *
 * @param string $entity_type
 * @param mixed $entity
 * @param string $lang_code
 */
function translation_aid_create_page($entity_type, $entity, $lang_code) {
// Load request parameters and set some variables
  global $user;
  list(, $destination) = TranslationAid::getEntityBasePaths($entity_type, $entity);
  if (isset($_GET['translator'])) {
    $translator = user_load($_GET['translator']);
  }
  else {
    $translator = $user;
  }

  // Call the creation function
  translation_aid_create_entity($entity_type, $entity, $lang_code, $translator);

  if (isset($destination)) {
    drupal_goto($destination);
  }
}